import { Component,ViewChild } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { User } from '../../user';
import { NgForm } from '@angular/forms';
import { ListaPage } from '../lista/lista';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  user: User = new User();
  @ViewChild('form') form: NgForm;

  constructor(
    public navCtrl: NavController,
    private toastCtrl: ToastController) {
  }

  login() {
    if (this.form.form.valid) {
      if(this.user.usuario == "admin" && this.user.password == "12"){

          this.navCtrl.setRoot(ListaPage);
        }else{

          let toast = this.toastCtrl.create({ duration: 3000, position: 'bottom' });

            toast.setMessage('Usuário ou senha inválida.');
            toast.present();
        }
      }
    }



}
