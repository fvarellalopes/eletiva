import { Component,ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-lista',
  templateUrl: 'lista.html'
})
export class ListaPage {
   public gifs = [];
   private url: string = "http://api.giphy.com/v1/gifs/search";
   txtPesquisa:string;
   private offset = 0;
   private key = 'ZjXKH7cVQWPKsLVPKxzkVvYZDPo9cBVR';
   @ViewChild('form') form: NgForm;


   constructor(public navCtrl: NavController, public http: Http) {


     }
     executarPesquisa(){
       this.http.get(this.url, {params:{'api_key':this.key, 'q':this.txtPesquisa, 'offset':this.offset, 'limit':5}})
       .map(res => res.json())
         .subscribe(data => {
           this.gifs= this.gifs.concat(data.data);
         });
     }
       buscar() {
         this.offset = 0;
         this.gifs=[];
         if (this.form.form.valid) {
           this.executarPesquisa();
           }
         }

       doInfinite(infiniteScroll) {
          this.offset +=5;
          this.executarPesquisa();
       }
}
